use std::collections::BTreeMap;

use serde::Deserialize;

pub mod yosys;

#[derive(Deserialize, Debug, Clone, Copy)]
pub struct FMax {
    pub achieved: f32,
    pub constraint: f32,
}

#[derive(Deserialize, Debug, Clone, Copy)]
pub struct Utilization {
    pub available: u32,
    pub used: u32,
}

#[derive(Deserialize, Debug)]
pub struct PnrReport {
    pub fmax: BTreeMap<String, FMax>,
    pub utilization: BTreeMap<String, Utilization>,
}

#[derive(Debug)]
pub struct SynthReport {
    pub cells_total: u32,
    pub per_cell_type: BTreeMap<String, u32>,
}
