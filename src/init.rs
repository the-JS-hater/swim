use std::process::Command;

use camino::{Utf8Path, Utf8PathBuf};
use color_eyre::{
    eyre::{anyhow, bail, Context, ContextCompat},
    Result,
};
use lazy_static::lazy_static;
use log::info;
use regex::Regex;

use crate::util::{copy_recurse, CommandExt};

const DEFAULT_CONFIG: &str = r#"name = "proj"
"#;

const DEFAULT_GITIGNORE: &str = r#"build/
"#;

const DEFAULT_SPADE: &str = r#"entity main() -> int<8> {
    0
}"#;

// NOTE: This contains a file system race so it can't be called in parallel (mainly, in multiple
// tests).
fn clone_template_repo(repo: &str) -> Result<Utf8PathBuf> {
    let template_dir: Utf8PathBuf = std::env::temp_dir()
        .join("swim-templates")
        .try_into()
        .context("temp dir path contains invalid UTF-8")?;
    if template_dir.exists() {
        // FIXME: If the dir exists, try to update it by running git-fetch.
        // NOTE: The dir might have been removed between checking if it exists and us
        // removing it here. Mostly relevant for tests since they run in parallel.
        std::fs::remove_dir_all(&template_dir)?;
    }

    // Clone the template repository
    Command::new("git")
        .args(["clone", repo, template_dir.as_ref()])
        .log_command()
        .status_and_log_output_if_tests()?
        .success()
        .then(|| ())
        .ok_or_else(|| anyhow!("Failed to clone template directory"))?;

    Ok(template_dir)
}

fn dir_name_to_project_name(name: &str) -> Result<String> {
    lazy_static! {
        static ref VALID_RE: Regex = Regex::new(
            r#"^(?x:
                [\p{XID_Start}_-]
                [\p{XID_Continue}-]*
                (\u{3F} | \u{21} | (\u{3F}\u{21}) | \u{2048})? # ? ! ?! ⁈
            )$"#
        )
        .unwrap();
    }

    if VALID_RE.is_match(name) {
        Ok(name.replace("-", "_"))
    } else {
        bail!("The project name ({name}) must be a valid Spade identifier")
    }
}

/// Initialize a spade project in the specified directory.
pub(crate) fn init_in_dir(
    path: impl AsRef<Utf8Path>,
    template_repo: &str,
    board: Option<&str>,
    lib_name: Option<String>,
) -> Result<()> {
    // Use the specified library name, or infer it from the directory name
    // if it is not set
    let lib_name = if let Some(lib_name) = lib_name {
        lib_name
    } else {
        dir_name_to_project_name(path.as_ref().file_stem().with_context(|| {
            anyhow!("Cannot initialize swim project in a directory with an empty name")
        })?)?
    };

    if !path.as_ref().join(".git").exists() {
        info!("Initialising git repo");
        Command::new("git")
            .arg("init")
            .current_dir(path.as_ref())
            .log_command()
            .status_and_log_output_if_tests()
            .context("Failed to run git init")?;
    }

    info!("Creating .gitignore");
    std::fs::write(path.as_ref().join(".gitignore"), DEFAULT_GITIGNORE)
        .context("Failed to write .gitignore")?;

    if let Some(board) = board {
        info!("Trying to copy template for {board}");
        let template_dir = clone_template_repo(template_repo)?;
        let board_dir = template_dir.join(board);
        if !board_dir.exists() {
            bail!("Couldn't find template for {}", board);
        }
        copy_recurse(template_dir.join(board), &path)
            .context("Failed to copy template directory")?;
    } else {
        info!("Creating default config and main.spade");

        std::fs::write(path.as_ref().join("swim.toml"), DEFAULT_CONFIG)
            .context("Failed to write default config")?;

        std::fs::create_dir_all(path.as_ref().join("src")).context("Failed to create src dir")?;
        std::fs::write(path.as_ref().join("src/main.spade"), DEFAULT_SPADE)
            .context("Failed to write initial spade code")?;
    }

    // Change the name of the project to the name lib_name
    let config_file = path.as_ref().join("swim.toml");
    let swim_toml_content = std::fs::read_to_string(&config_file)
        .with_context(|| anyhow!("Faield to open swim.toml in newly created project"))?;
    let new_content =
        swim_toml_content.replace(r#"name = "proj""#, &format!(r#"name = "{lib_name}""#));
    std::fs::write(&config_file, new_content)
        .with_context(|| format!("Failed to write swim.toml to {config_file}"))?;

    Ok(())
}

pub fn init(dir: Option<&Utf8Path>, template_repo: &str, board: Option<&str>) -> Result<()> {
    let dir = match dir {
        Some(dir) => dir.to_path_buf(),
        None => std::env::current_dir()?.try_into()?,
    };

    if dir.exists() {
        // Check for existing non-hidden files. We ignore errors since we'd rather have false
        // positives than false negatives. (False positive means that we think we can initialize,
        // try, and find out that there is something wrong with the directory. False negative means
        // that we think we can't initialize so we stop, when we actually would've succeeded.)
        // Items in the iterator are Err if there are "intermittent IO errors" which seem difficult
        // to do much more with than ignore.
        if dir
            .read_dir()
            .with_context(|| format!("Failed to read dir {dir}"))?
            .filter_map(|file| file.ok())
            .any(|file| !file.file_name().to_string_lossy().starts_with('.'))
        {
            bail!("Directory is not empty");
        }

        let init_result = init_in_dir(&dir, template_repo, board, None);
        if init_result.is_ok() {
            info!("Initialised spade project in {dir:?}");
        }
        init_result
    } else {
        std::fs::create_dir_all(&dir)
            .with_context(|| format!("Failed to create directory {dir:?}"))?;
        let init_result = init_in_dir(&dir, template_repo, board, None);
        if init_result.is_ok() {
            info!("Initialised spade project in {dir:?}");
        } else {
            info!("Removing newly created dir due to failure");
            std::fs::remove_dir_all(&dir).context("Failed to remove {dir:?}")?;
        }
        init_result
    }
}

pub fn list_boards(template_repo: &str) -> Result<()> {
    let template_dir = clone_template_repo(template_repo)?;
    let mut dirs: Vec<_> = template_dir
        .read_dir()?
        .filter_map(Result::ok)
        .filter(|e| e.path().is_dir())
        .map(|dir| dir.file_name().to_string_lossy().into_owned())
        .filter(|s| !s.starts_with('.'))
        .collect();

    dirs.sort();
    info!("Available boards:\n{}", dirs.join("\n"));
    Ok(())
}

#[cfg(test)]
mod tests {
    use predicates::prelude::*;
    use std::collections::HashSet;

    use crate::test::git;

    #[test]
    fn init_creates_required_files() {
        let temp = assert_fs::TempDir::new().unwrap();
        let dir = temp.join("swim-init-test");
        let dir_utf8: camino::Utf8PathBuf = dir.clone().try_into().unwrap();

        let initial_dir = std::env::current_dir().unwrap();

        crate::init::init(
            Some(&dir_utf8),
            "https://gitlab.com/spade-lang/swim-templates",
            None,
        )
        .unwrap();

        let new_dir = std::env::current_dir().unwrap();

        assert_eq!(new_dir, initial_dir, "Init command changed directory");

        // Check that files have been created.
        let wanted_files =
            HashSet::from([dir.join("swim.toml"), dir.join("src").join("main.spade")]);

        for file in &wanted_files {
            assert!(predicate::path::exists().eval(file));
        }
    }

    #[test]
    fn init_board_creates_required_files() {
        let temp = assert_fs::TempDir::new().unwrap();
        let dir = temp.join("swim-init-test");
        let dir_utf8: camino::Utf8PathBuf = dir.clone().try_into().unwrap();

        let initial_dir = std::env::current_dir().unwrap();

        crate::init::init(
            Some(&dir_utf8),
            "https://gitlab.com/spade-lang/swim-templates",
            Some("ecpix5"),
        )
        .unwrap();

        let new_dir = std::env::current_dir().unwrap();

        assert_eq!(new_dir, initial_dir, "Init command changed directory");

        // Check that files have been created.
        let wanted_files =
            HashSet::from([dir.join("swim.toml"), dir.join("src").join("main.spade")]);

        for file in &wanted_files {
            assert!(predicate::path::exists().eval(file));
        }
    }

    #[test]
    fn git_init_then_swim_init_works() {
        let temp = assert_fs::TempDir::new().unwrap();
        let dir = temp.join("swim-init-test");
        let dir_utf8: camino::Utf8PathBuf = dir.clone().try_into().unwrap();

        std::fs::create_dir(&dir).unwrap();
        git::init(&dir);

        crate::init::init(
            Some(&dir_utf8),
            "https://gitlab.com/spade-lang/swim-templates",
            None,
        )
        .unwrap();

        // Check that files have been created.
        let wanted_files =
            HashSet::from([dir.join("swim.toml"), dir.join("src").join("main.spade")]);

        for file in &wanted_files {
            assert!(predicate::path::exists().eval(file));
        }
    }

    #[test]
    fn init_board_correctly_sets_project_name() {
        let temp = assert_fs::TempDir::new().unwrap();
        let dir = temp.join("swim-init-test");
        let dir_utf8: camino::Utf8PathBuf = dir.clone().try_into().unwrap();

        let initial_dir = std::env::current_dir().unwrap();

        crate::init::init(
            Some(&dir_utf8),
            "https://gitlab.com/spade-lang/swim-templates",
            Some("ecpix5"),
        )
        .map_err(|e| println!("{e:#?}"))
        .expect("Failed to init from template");

        let new_dir = std::env::current_dir().unwrap();

        assert_eq!(new_dir, initial_dir, "Init command changed directory");

        // Check that the project name is what we expect

        let toml_content =
            std::fs::read_to_string(dir.join("swim.toml")).expect("did not find swim.toml");
        let t = toml::from_str::<crate::config::Config>(&toml_content)
            .expect("Failed to decode swim.toml");
        assert_eq!(t.name, "swim_init_test");
    }

    #[test]
    fn init_sets_project_name() {
        let temp = assert_fs::TempDir::new().unwrap();
        let dir = temp.join("swim-init-test");
        let dir_utf8: camino::Utf8PathBuf = dir.clone().try_into().unwrap();

        let initial_dir = std::env::current_dir().unwrap();

        crate::init::init(
            Some(&dir_utf8),
            "https://gitlab.com/spade-lang/swim-templates",
            None,
        )
        .unwrap();

        let new_dir = std::env::current_dir().unwrap();

        assert_eq!(new_dir, initial_dir, "Init command changed directory");

        // Check that the project name is what we expect

        let toml_content =
            std::fs::read_to_string(dir.join("swim.toml")).expect("did not find swim.toml");
        let t = toml::from_str::<crate::config::Config>(&toml_content)
            .expect("Failed to decode swim.toml");
        assert_eq!(t.name, "swim_init_test");
    }

    #[test]
    fn init_with_bad_name_is_an_error() {
        let temp = assert_fs::TempDir::new().unwrap();
        let dir = temp.join("2swim-init-test");
        let dir_utf8: camino::Utf8PathBuf = dir.clone().try_into().unwrap();

        let result = crate::init::init(
            Some(&dir_utf8),
            "https://gitlab.com/spade-lang/swim-templates",
            None,
        )
        .map_err(|e| format!("{e}"));
        assert_eq!(
            result,
            Err("The project name (2swim-init-test) must be a valid Spade identifier".to_string())
        )
    }
}
