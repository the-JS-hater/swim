use std::process::Command;

use camino::Utf8Path;
use color_eyre::eyre::{bail, Context, Result};
use log::info;

/// Runs the specified commands in order, returning Ok if all commands succeeded
/// and Err if not. The working directory of the commands is set to root_dir
pub fn perform_preprocessing(root_dir: impl AsRef<Utf8Path>, commands: &[String]) -> Result<()> {
    for command in commands {
        info!("Running preprocessing command '{command}'");
        let status = Command::new("sh")
            .arg("-c")
            .arg(&command)
            .current_dir(root_dir.as_ref())
            .status()
            .with_context(|| format!("Faield to run {command}"))?;

        if !status.success() {
            bail!("Preprocessing '{command}' exited with error code")
        }
    }
    Ok(())
}
