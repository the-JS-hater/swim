use std::collections::{HashMap, HashSet};

use crate::{
    build_dir, compiler_dir,
    config::Config,
    util::{make_relative, pluralize},
};

use camino::{Utf8Path, Utf8PathBuf};
use color_eyre::{
    eyre::{bail, Context},
    Result,
};
use itertools::Itertools;
use log::warn;
use serde::Deserialize;

use super::variables::{translate_variables, PluginContext};

#[derive(Debug, Deserialize, PartialEq, Eq)]
pub enum BuildStep {
    /// Before any other processing takes place
    Start,
    SpadeBuild,
    Simulation,
    Synthesis,
    Pnr,
    Upload,
}

#[derive(Debug, Deserialize)]
pub struct BuildResult {
    /// The path of a file built by this build step
    pub path: String,
    /// The first build step for which this file is required. This will trigger
    /// a re-build of this build step if the file was changed
    pub needed_in: BuildStep,
}

impl BuildResult {
    pub fn replace_variables(&mut self, plugin_ctx: &PluginContext, config: &Config) -> Result<()> {
        let Self { path, needed_in: _ } = self;
        translate_variables(path, plugin_ctx, config).context("In path {path}")
    }
}

#[derive(Debug, Deserialize)]
pub struct SynthesisConfig {
    /// Yosys commands to run after the normal yosys flow
    pub yosys_post: Vec<String>,
}

impl SynthesisConfig {
    pub fn replace_variables(&mut self, plugin_ctx: &PluginContext, config: &Config) -> Result<()> {
        let Self { yosys_post } = self;
        yosys_post
            .iter_mut()
            .map(|command| {
                translate_variables(command, plugin_ctx, config)
                    .with_context(|| format!("In command {command}"))
            })
            .collect::<Result<()>>()
            .context("In yosys post command list")
    }
}

#[derive(Debug, Deserialize)]
pub struct PluginCommand {
    /// List of system commands to run in order to execute the command
    ///
    /// Commands specified by the user, i.e. whatever is after `swim plugin <command>`
    /// is string replaced into `%args%` in the resulting command string. The arguments
    /// are passed as strings, to avoid shell expansion
    pub script: Vec<String>,
    /// The build step after which to run this command
    pub after: BuildStep,
}

#[derive(Debug, Deserialize)]
pub struct PluginConfig {
    /// True if this plugin needs the CXX bindings for the spade compiler to be built
    #[serde(default)]
    pub requires_cxx: bool,
    /// Commands required to build the plugin. Run before any project compilation steps
    #[serde(default)]
    pub build_commands: Vec<String>,
    /// The files which this plugin produces
    #[serde(default)]
    pub builds: Vec<BuildResult>,

    /// Arguments which must be set in the `swim.toml` of projects using the plugin
    #[serde(default)]
    pub required_args: HashSet<String>,

    /// Commands to run after building swim file but before anything else
    #[serde(default)]
    pub post_build_commands: Vec<String>,

    /// Things to do during the synthesis process
    pub synthesis: Option<SynthesisConfig>,

    /// Commands which the user can execute
    #[serde(default)]
    pub commands: HashMap<String, PluginCommand>,
}

impl PluginConfig {
    pub fn load(
        name: &str,
        plugin_dir: &Utf8Path,
        root_dir: &Utf8Path,
        config: &Config,
        args: &HashMap<String, String>,
    ) -> Result<Self> {
        let plugin_ctx = PluginContext {
            plugin_dir: plugin_dir.to_owned(),
            compiler_dir: compiler_dir(root_dir, &config.compiler),
            root_dir: root_dir.to_owned(),
            build_dir: build_dir(root_dir),
            args: args.clone(),
        };

        let toml_file = make_relative(&plugin_dir.join("swim_plugin.toml"));

        if !toml_file.exists() {
            bail!("{plugin_dir} does not contain a swim_plugin.toml");
        }

        let toml_content = std::fs::read_to_string(&toml_file)
            .with_context(|| format!("Failed to read {toml_file}"))?;

        let toml_de = toml::Deserializer::new(&toml_content);
        let mut unknown_fields = std::collections::HashSet::new();
        let mut result = serde_ignored::deserialize(toml_de, |path| {
            unknown_fields.insert(path.to_string());
        })
        .with_context(|| format!("Failed to decode toml in {toml_file}"))?;
        if !unknown_fields.is_empty() {
            warn!(
                "{toml_file} contains {} unknown field{}:",
                unknown_fields.len(),
                pluralize(unknown_fields.len())
            );
            for field in &unknown_fields {
                warn!("  {}", field);
            }
        }

        let PluginConfig {
            requires_cxx: _,
            build_commands,
            builds,
            post_build_commands,
            synthesis,
            required_args,
            commands,
        } = &mut result;

        let arg_names = args.keys().cloned().collect::<HashSet<_>>();
        let extra_args = arg_names.difference(&required_args).collect::<Vec<_>>();
        match extra_args.as_slice() {
            [] => {}
            [single] => bail!("Plugin {name} has no arguemnt '{single}'"),
            _ => bail!(
                "Plugin {name} does not take the arguments [{}]",
                extra_args.iter().map(|arg| format!("'{arg}'")).join(", ")
            ),
        }
        let missing_args = required_args.difference(&arg_names).collect::<Vec<_>>();
        match missing_args.as_slice() {
            [] => {}
            [single] => bail!("Plugin {name} requires the argument '{single}' to be set"),
            _ => bail!(
                "Plugin {name} requires the arguments [{}] to be set",
                missing_args.iter().map(|arg| format!("'{arg}'")).join(", ")
            ),
        }

        build_commands
            .iter_mut()
            .map(|c| translate_variables(c, &plugin_ctx, config).with_context(|| format!("in {c}")))
            .collect::<Result<Vec<()>>>()
            .with_context(|| format!("In 'commands' list in {toml_file}"))?;

        post_build_commands
            .iter_mut()
            .map(|c| translate_variables(c, &plugin_ctx, config).with_context(|| format!("In {c}")))
            .collect::<Result<Vec<()>>>()
            .with_context(|| format!("In 'post_build_commands' list in {toml_file}"))?;

        builds
            .iter_mut()
            .map(|b| b.replace_variables(&plugin_ctx, config))
            .collect::<Result<Vec<()>>>()
            .with_context(|| format!("In 'builds' list in {toml_file}"))?;

        commands
            .iter_mut()
            .map(|(name, c)| {
                let PluginCommand { after: _, script } = c;
                script
                    .iter_mut()
                    .map(|a| {
                        translate_variables(a, &plugin_ctx, config)
                            .with_context(|| format!("In {a}"))
                    })
                    .collect::<Result<()>>()
                    .with_context(|| format!("In script for command {name}"))
            })
            .collect::<Result<_>>()?;

        if let Some(synthesis) = synthesis {
            synthesis
                .replace_variables(&plugin_ctx, config)
                .with_context(|| format!("In synthesis block of {toml_file}"))?;
        }

        Ok(result)
    }

    /// List of files which trigger a rebuild on the speicified step if they change
    pub fn rebuild_list(&self, build_step: &BuildStep) -> Vec<Utf8PathBuf> {
        self.builds
            .iter()
            .filter_map(|f| {
                if build_step == &f.needed_in {
                    Some(f.path.clone().into())
                } else {
                    None
                }
            })
            .collect()
    }
}
