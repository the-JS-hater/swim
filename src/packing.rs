use std::process::Command;

use camino::Utf8PathBuf;
use color_eyre::eyre::{anyhow, Context, Result};
use log::info;

use crate::{
    build_dir, config,
    pnr::{run_pnr, PnrResult},
    util::needs_rebuild,
    CommandCtx,
};

pub enum PackingResult {
    Bin(Utf8PathBuf),
    Svf(Utf8PathBuf),
}

impl PackingResult {
    pub fn file(&self) -> Utf8PathBuf {
        match self {
            PackingResult::Bin(f) => f.clone(),
            PackingResult::Svf(f) => f.clone(),
        }
    }
}

pub fn run_packing(ctx: &CommandCtx) -> Result<PackingResult> {
    let CommandCtx {
        root_dir,
        args: _,
        config,
        compiler: _,
        plugins: _,
    } = ctx;
    let pnr_result = run_pnr(ctx, false)?;
    let packing_config = config.packing_config()?;

    match packing_config.as_ref() {
        config::PackingTool::Icepack => {
            let asc_file = match pnr_result {
                PnrResult::Asc(asc_file) => asc_file,
                _ => {
                    return Err(anyhow!(
                        "icepack only supports asc-files. Use another pnr or packing-tool"
                    ))
                }
            };

            let bin_file = build_dir(root_dir).join("hardware.bin");
            if needs_rebuild(&bin_file, [&asc_file].into_iter())? {
                info!("Packing bin-file with icepack");

                let status = Command::new("icepack")
                    .arg(&asc_file)
                    .arg(&bin_file)
                    .status()
                    .context("Failed to run icepack")?;

                if status.success() {
                    Ok(PackingResult::Bin(bin_file))
                } else {
                    Err(anyhow!("Failed to pack bin-file using icepack"))
                }
            } else {
                Ok(PackingResult::Bin(bin_file))
            }
        }

        config::PackingTool::Ecppack { idcode } => {
            let textcfg_file = match pnr_result {
                PnrResult::Textcfg(textcfg_file) => textcfg_file,
                _ => return Err(anyhow!("ecppack only supports .config-files")),
            };

            let svf_file = build_dir(root_dir).join("hardware.svf");
            if needs_rebuild(&svf_file, [&textcfg_file].into_iter())? {
                info!("Packing svf-file using ecppack");

                let status = Command::new("ecppack")
                    .args(if let Some(id) = idcode {
                        vec!["--idcode", id]
                    } else {
                        vec![]
                    })
                    .arg("--input")
                    .arg(textcfg_file)
                    .arg("--svf")
                    .arg(&svf_file)
                    .status()
                    .context("Failed to run ecppack")?;

                if status.success() {
                    info!("Packing succesful");
                    Ok(PackingResult::Svf(svf_file))
                } else {
                    Err(anyhow!("Failed to pack svf-file using ecppack"))
                }
            } else {
                Ok(PackingResult::Svf(svf_file))
            }
        }
    }
}
