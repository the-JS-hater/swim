#pragma once

#include <verilated.h>
#include <vector>
#include <string>
#include <iostream>
#include <optional>
#include <spade.rs.h>
#include <verilator_wrapper.hpp>

#define STRINGIFY(arg) STRINGIFY_(arg)
#define STRINGIFY_(arg) #arg
#define ID(x) x

#define CONCAT_(l, r) l ## r
#define CONCAT(l, r) CONCAT_(l, r)


#ifndef TOP
    #error "TOP must be set before including" __FILE__
#endif

#include STRINGIFY(ID(V)TOP.h)

#define ASSERT_EQ(FIELD, EXPECTED) \
    FIELD->assert_eq(EXPECTED, __FILE__  ":" STRINGIFY(__LINE__));

namespace verilator_util {
    using top_t = CONCAT(V, TOP);
    using spade_wrapper = CONCAT(TOP, _spade_t);

    template<typename T, typename S>
    class TestCase {
        public:
            virtual std::string name() = 0;
            virtual int run(T* dut, S& s, VerilatedContext* ctx) = 0;

            int run_internal(int argc, char** argv, std::string spade_top, std::string spade_state_file) {
                auto contextp = std::make_unique<VerilatedContext>();

                contextp->traceEverOn(true);

                // Pass arguments so Verilated code can see them, e.g. $value$plusargs
                // This needs to be called before you create any model
                contextp->commandArgs(argc, argv);

                // Construct the Verilated model, from Vmain.h generated from Verilating "top.v"

                const std::unique_ptr<T> top{new T{contextp.get(), ""}};
                // auto top = std::make_unique<T>(contextp.get(), "");

                auto s = spade_wrapper(spade_state_file, spade_top, top.get());

                auto run_result = this->run(top.get(), s, contextp.get());

                top->final();

                return run_result;
            }
    };

    class TestList {
        public:
            void register_test(TestCase<top_t, spade_wrapper>* t) {
                this->tests.push_back(t);
            }

            // NOTE: This leaks like crazy, but the test case instances are small and
            // live the whole program, so we're fine™️
            std::vector<TestCase<top_t, spade_wrapper>*> tests;
    };

    // https://isocpp.org/wiki/faq/ctors#static-init-order
    TestList* test_list() {
        static verilator_util::TestList* test_list = new verilator_util::TestList();
        return test_list;
    }
}


#define TEST_CASE(TEST_NAME, body) \
    class TEST_NAME : public ::verilator_util::TestCase<verilator_util::top_t, verilator_util::spade_wrapper> { \
        public: \
            TEST_NAME () { \
                verilator_util::test_list()->register_test(this); \
            } \
            std::string name() override { \
                return STRINGIFY(TEST_NAME); \
            } \
            \
            int run(verilator_util::top_t* dut, verilator_util::spade_wrapper& s, VerilatedContext* ctx) override body \
    }; \
    TEST_NAME CONCAT(TEST_NAME, _instance);


#define KWARG(name, var) \
    if (std::string(argv[i]) == std::string(name)) { \
        if (i+1 == argc) { \
            std::cerr << name << " specified without specifying a test" << std::endl; \
            return 1; \
        } \
        else { \
            var = argv[i+1]; \
        } \
    }
#define REQUIRE_ARG(name, var) \
    if (!var.has_value()) { \
        std::cerr << name << " was not specified" << std::endl; \
        return 1; \
    }



int main_impl(int argc, char** argv) {
    std::optional<std::string> target_test;
    std::optional<std::string> top;
    std::optional<std::string> spade_state;
    for(int i = 0; i < argc; i++) {
        KWARG("--test", target_test);
        KWARG("--spade-top", top);
        KWARG("--spade-state", spade_state)
    }

    REQUIRE_ARG("--test", target_test);
    REQUIRE_ARG("--spade-top", top);
    REQUIRE_ARG("--spade-state", spade_state)

    for (auto&& test : verilator_util::test_list()->tests) {
        if (test->name() == target_test) {
            return test->run_internal(argc, argv, top.value(), spade_state.value());
        }
    }
    std::cerr << "The specified test (" << target_test.value() << ") was not found" << std::endl;
    return 2;
}

#define MAIN \
    int main(int argc, char** argv) { \
        return main_impl(argc, argv); \
    }
